Many thanks to learnopengl.com for a great tutorial (this project wouldn't be possible without it)

Building:

All:
	Install cglm, glfw

Arch:
	 <code>
	 yay -S base-devel cglm glfw python-pip
	 </code>

Ubuntu/Debian:
	 <code>
	 apt install build-essential git cmake python3-pip libglfw3-dev
	 </code>
	 Install cglm from [git](https://github.com/recp/cglm) repository

Fedora:
	<code>
	dnf install glfw-devel
	</code>
	Install cglm from [git](https://github.com/recp/cglm) repository

On every distro:
	<code>
	pip install glad
	glad --generator c --out-path /usr
	make
	</code>

On Windows/MinGW:
	<br>
	1. Download MinGW and other libs<br>
	2. Change Makefile<br>
	3. Compile<br>
