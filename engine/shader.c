#include <stdlib.h>
#include <stdio.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "util.h"
#include "shader.h"

unsigned int createShaderProgram(const char *vertexSourceFile, const char* fragmentSourceFile)
/* reads files which contain shader sources and creates a shader program */
{
	unsigned int shaderProgram;
	int success;
	char infolog[512];

	/* get sources */
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	char *vsource = strcat_malloc(getCWD(), vertexSourceFile);
	const char *vertexSource = readFile(vsource);
	free(vsource);

	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	char *fsource = strcat_malloc(getCWD(), fragmentSourceFile);
	const char *fragmentSource = readFile(fsource);
	free(fsource);

	if (vertexSource == NULL || fragmentSource == NULL)
		ERROR("Failed to load vertex or fragment source");

	glShaderSource(vertexShader, 1, &vertexSource, NULL);
	glShaderSource(fragmentShader, 1, &fragmentSource, NULL);

	/* vertex */
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if ( !success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infolog);
		ERROR("Vertex shader compilation failed:\n%s\n%s", infolog, vertexSource);
	}

	/* fragment */
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if ( !success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infolog);
		ERROR("Fragment shader compilation failed:\n%s\n%s", infolog, fragmentSource);
	}

	/* linking vertex and fragment shader to shader program */
	shaderProgram = glad_glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if ( !success)
	{
		glGetProgramInfoLog(shaderProgram, 512, NULL, infolog);
		ERROR("Shader program compilation failed:\n%s", infolog);
	}
	free((char*)vertexSource);
	free((char*)fragmentSource);

	return shaderProgram;
}
