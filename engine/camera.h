#ifndef CAMERA_H
#define CAMERA_H

#include <cglm/cglm.h>

typedef struct Camera{
	vec3 position;
	vec3 rotation; // x=yaw, y=pitch, z=roll
	vec3 target;
	vec3 direction;
	vec3 right;
	vec3 up;
	vec3 front;
	mat4 view;
	mat4 projection;
}Camera;

Camera *camera_create(float *position);
void camera_lookat(Camera *c, float *target);
void camera_translate(Camera *c, float *new_position);
#endif
