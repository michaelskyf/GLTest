#ifndef UTILS_H
#define UTILS_H

#include "object.h"

#define LOG(fmt, ...) fprintf(stdout, "[INF] " fmt "\n", ##__VA_ARGS__)
#define LOGW(fmt, ...) fprintf(stdout, "[WARN] " fmt "\n", ##__VA_ARGS__)
#define LOGD(fmt, ...) fprintf(stdout, "[DBG] " fmt "\n", ##__VA_ARGS__)
#define LOGE(fmt, ...) fprintf(stderr, "[ERR] " fmt "\n", ##__VA_ARGS__)
#define ERROR(...) {LOGE(__VA_ARGS__); glfwTerminate(); exit(-1);}

typedef struct Settings{

        /* display */
        int width;
        int height;
        void *fullscreen;

        /* quality */
        unsigned int msaa;

        /* camera settings */
        float fov;
        int far;
        float sensitivity;

} Settings;

char *readFile(const char *fname);
char *getCWD();
char *strcat_malloc(const char *str1, const char *str2);
Settings parseConfig(const char *fname);
Mesh *parseMesh(const char *fname);
unsigned int loadTexture(const char *fname);

#endif
