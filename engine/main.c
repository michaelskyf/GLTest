#include <cglm/affine-mat.h>
#include <cglm/affine.h>
#include <cglm/cam.h>
#include <cglm/mat4.h>
#include <cglm/project.h>
#include <cglm/types.h>
#include <cglm/util.h>
#include <cglm/vec3.h>
#include <cglm/vec4.h>
#include <cglm/cglm.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "util.h"
#include "object.h"
#include "camera.h"
#include "vector.h"
#include "shader.h"
#include "engine.h"

/* global variables */
EngineVars engineVars;

Settings settings;
Camera *camera;
GLFWwindow *window;
Vector *shaderList;
Vector *cameraList;

/* callback functions */

void framebuffer_size_callback(GLFWwindow *window, int width, int height)
/* gets called on window resize */
{
	glViewport(0, 0, width, height);
	if (settings.fullscreen){
		return;
	}

	settings.width = width;
	settings.height = height;
	glm_perspective_resize((float)width/height, camera->projection);
}

void mouse_callback(GLFWwindow *window, double x, double y)
{
	static float lastX;
	static float lastY;

	float xoffset = (x - lastX) * settings.sensitivity;
	float yoffset = (lastY - y) * settings.sensitivity;

	lastX = x;
	lastY = y;

	camera->rotation[0] += xoffset;
	camera->rotation[1] += yoffset;

	if (camera->rotation[1] > 89.0f)
		camera->rotation[1] = 89.0f;
	if (camera->rotation[1] < -89.0f)
		camera->rotation[1] = -89.0f;

	camera->direction[0] = cos(glm_rad(camera->rotation[0])) * cos(glm_rad(camera->rotation[1]));
	camera->direction[1] = sin(glm_rad(camera->rotation[1]));
	camera->direction[2] = sin(glm_rad(camera->rotation[0])) * cos(glm_rad(camera->rotation[1]));
	glm_normalize(camera->direction);
	memcpy(camera->front, camera->direction, sizeof(vec3));
}

void setup()
{
	/* load settings from file */
	LOG("Loading settings from settings.conf");

	char *fname = strcat_malloc(getCWD(), "settings.conf");
	settings = parseConfig(fname);
	free(fname);

	/* initialize essential variables */
	engineVars.camera = &camera;
	engineVars.settings = &settings;
	engineVars.shaderList = &shaderList;
	engineVars.window = &window;

	/* setup lists */
	shaderList = vector_create(0, sizeof(Shader));

	/* setup opengl */
	LOG("Initializing openGL");
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, settings.msaa);

	if (settings.fullscreen)
		settings.fullscreen = glfwGetPrimaryMonitor();
	else
		settings.fullscreen = NULL;

	/* create window */
	window = glfwCreateWindow(settings.width,
			settings.height, "GLTest", settings.fullscreen, NULL);

	if (window == NULL)
		ERROR("Failed to create GLFW window");

	glfwMakeContextCurrent(window);

	/* initialize GLAD */
	if ( !gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		ERROR("Failed to initialize GLAD");

	/* set the size of the rendering window */
	glViewport(0, 0, settings.width, settings.height);

	/* multisampling */
	glEnable(GL_MULTISAMPLE);

	/* set clear color */
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

	/* depth testing */
	glEnable(GL_DEPTH_TEST);

	/* set texture wrapping */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	/* set texture filtering */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	/* create mipmaps */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	stbi_set_flip_vertically_on_load(true);

	LOG("OpenGL setup completed");

	/* disable cursor */
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	/* setup shaders */
	int shaderId = vector_create_element(shaderList);
	Shader *shader = vector_get_element(shaderList, shaderId);
	shader->objectList = vector_create(0, sizeof(Object));
	shader->ID = createShaderProgram("shaders/default.vs", "shaders/default.fs");

	/* create camera */
	cameraList = vector_create(1, sizeof(Camera));
	int cameraId = vector_create_element(cameraList);
	camera = vector_get_element(cameraList, cameraId);
	Camera *cp = camera_create((vec3){0,0,0});
	memcpy(camera, cp, sizeof(Camera));
	free(cp);

	/* set projection */
	glm_perspective(glm_rad(settings.fov),
			(float)settings.width/settings.height,
			0.1f, settings.far, camera->projection);

	/* register callback functions */
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);

	/* program startup */
	LOG("Starting program setup");
	start();
	LOG("Program setup comleted");
}

/* -------------------------------------------------------------------------------- */

void draw()
{
	/* for active camera */
	camera = vector_get_element(cameraList, 0);
	vec3 look;
	glm_vec3_add(camera->position, camera->front, look);
	camera_lookat(camera, look);

	mat4 clip;
	glm_mul(camera->projection, camera->view, clip);


	/* for each shader program */
	for (int si = 0; si < vector_size(shaderList); si++)
	{
		Shader *s = vector_get_element(shaderList, si);
		glUseProgram(s->ID);

		int clipLoc = glad_glGetUniformLocation(s->ID, "clip");
		glUniformMatrix4fv(clipLoc, 1, GL_FALSE, *clip);

		/* for each object */
		for (int oi = 0; oi < vector_size(s->objectList); oi++)
		{
			Object *o = vector_get_element(s->objectList, oi);
			glBindVertexArray(o->vao);

			/* for each instance */
			for (int ii = 0; ii < vector_size(o->instanceList); ii++)
			{
				int modelLoc = glGetUniformLocation(s->ID, "model");
				glUniformMatrix4fv(modelLoc, 1, GL_FALSE, vector_get_element(o->instanceList, ii));
				glDrawElements(GL_TRIANGLES, o->mesh->isize, GL_UNSIGNED_SHORT, 0);
			}
		}
	}
}


int main ( int argc, const char **argv )
{
	LOG("Starting setup");
	setup();
	LOG("Setup completed, starting main loop");

	float currentTime = 0;
	float lastTime = 0;

	/* render loop */
	while ( !glfwWindowShouldClose(window))
	{
		currentTime = glfwGetTime();
		engineVars.deltaTime = currentTime-lastTime;

		glfwPollEvents();

		/* update */
		update();

		/* render */
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		draw();

		/* swap buffers and poll IO events */
		glfwSwapBuffers(window);

		lastTime = currentTime;
	}
	LOG("Exiting");
	glfwTerminate();
	return 0;
}
