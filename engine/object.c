#include <cglm/affine.h>
#include <cglm/mat4.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <cglm/cglm.h>
#include <pthread.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "object.h"
#include "vector.h"
#include "util.h"

void object_destroy(Object *o)
{
	free(o->mesh->indices);
	free(o->mesh->vertices);
	free(o->mesh);

	vector_destroy(o->instanceList);
	free(o);
}

Object* object_create ()
{
	Object *o = malloc(sizeof(Object));

	glGenBuffers(1, &o->vbo);
	glGenBuffers(1, &o->ebo);
	glad_glGenVertexArrays(1, &o->vao);

	o->instanceList = vector_create(0, sizeof(Instance));

	return o;
}

void object_init (Object *o, Mesh *m)
{
	o->mesh = m;

	glBindVertexArray(o->vao);
	glBindBuffer(GL_ARRAY_BUFFER, o->vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, o->ebo);

	glBufferData(GL_ARRAY_BUFFER, o->mesh->vsize * sizeof(Vertex), o->mesh->vertices, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, o->mesh->isize * sizeof(*o->mesh->indices), o->mesh->indices, GL_STATIC_DRAW);

	/* Linking vertex attributes */
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(0);

	//glBindTexture(GL_TEXTURE_2D, o->mesh->texture);
	glBindTexture(GL_TEXTURE_2D, o->texture);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)sizeof(o->mesh->vertices[0].position));
	glEnableVertexAttribArray(1);
}

unsigned int object_instance_create(Object *o)
{
	return vector_create_element(o->instanceList);
}

void object_instance_scale(Instance *i, vec3 scalar)
{
	mat4 scale;
	glm_scale_make(scale, scalar);
	glm_mat4_mul(i->model, scale, i->model);
}

void object_instance_move(Instance *i, vec3 vector)
{
	glm_translate(i->model, vector);
}

void object_instance_move_make(Instance *i, vec3 vector)
{
	glm_translate_make(i->model, vector);
}

void object_instance_destroy(Object *o, void *element)
{
	vector_pop(o->instanceList, element);
}
