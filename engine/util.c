#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <glad/glad.h>

#if defined(__unix__)
#include <unistd.h>
#endif

#if defined(__linux__)
#include <linux/limits.h>
#endif

#if defined(__WIN32)
#include <direct.h>
#endif

#include "util.h"
#include "object.h"
#include "vector.h"
#include "engine.h"

#include "stb_image.h"

typedef struct Parameter {
	char *name;
	char *fmt;
	char *def_val;
	char *delim;
	Vector *target;
	int set;
}Parameter;


char *readFile(const char *fname)
{
	int fsize = 0;
	unsigned int rcount = 0;

	FILE *fp = fopen(fname, "rb");
	if (!fp) {
		return NULL;
	}

	/* Calculate file size */
	fseek(fp, 0L, SEEK_END);
	fsize = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	char *ret = calloc(fsize+1, sizeof(char));
	rcount = fread(ret, sizeof(char), fsize, fp);
	if (rcount < fsize) {
		free(ret);
		ret = NULL;
	}
	return ret;
}

char *getCWD()
{
	static char *path;

	if(path != 0)
		return path;

	path = malloc(PATH_MAX);
	path = getcwd(path, PATH_MAX);

	unsigned int plen = strlen(path) + 1;
	path[plen-1] = '/';
	path[plen++] = '\0';

	path = realloc(path, plen);

	return path;
}

char *strcat_malloc(const char *str1, const char *str2)
{
	char *ret = calloc(1, strlen(str1) + strlen(str2) + 1);

	strcat(ret, str1);
	strcat(ret, str2);

	return ret;
}

void findValue(const char *src, const char *fmt, Vector *dest_vec)
{
	while (strlen(fmt) != 0){

		unsigned int offset_tmp;
		char tmp[8];

		void *dest = vector_get_element(dest_vec, vector_create_element(dest_vec));

		sscanf(fmt, "%s%n", tmp, &offset_tmp);
		strcat(tmp, "%n");
		fmt += offset_tmp;

		sscanf(src, tmp, dest, &offset_tmp);
		src += offset_tmp;
	}
}


void parse(const char *fname, Parameter *paramArray, size_t nelems)
{

	FILE *fp = fopen(fname, "rb");

	if (fp == NULL){
		LOGE("Failed to open file %s.", fname);
		goto def_val;
	}

	char buffer[128];

	while (fgets(buffer, sizeof(buffer), fp)) {

		for(int i = 0; i < nelems; i++){
			if (!strncmp(buffer, paramArray[i].name, strlen(paramArray[i].name))){

				if(paramArray[i].delim != NULL){
					char *tmp = NULL;
					while((tmp = strstr(buffer, paramArray[i].delim)) != NULL)
						for(int ii = 0; ii < strlen(paramArray[i].delim); ii++)
							tmp[ii] = ' ';
				}

				findValue(buffer + strlen(paramArray[i].name), paramArray[i].fmt, paramArray[i].target);
					paramArray[i].set = 1;
			}
		}
		memset(buffer, 0, sizeof(buffer));
	}
def_val:
	// default value
	for(int i = 0; i < nelems; i++){
		if (paramArray[i].set == 1)
			continue;

		if (paramArray[i].def_val != NULL){
			findValue(paramArray[i].def_val, paramArray[i].fmt, paramArray[i].target);
			LOGD("Parameter \"%s\" uses default value", paramArray[i].name);
		} else {
			LOGD("%s is NULL", paramArray[i].name);
		}
	}

}

Settings parseConfig(const char *fname)
{
	Parameter paramArray[] = {
		/* name 	fmt		def_val*/
		//Display
		{"resolution ", "%d %d",	"800 600"	},
		{"fullscreen ", "%d",	    	"0"		},
		//Quality
		{"msaa ", 	"%d",	    	"0"		},
		//Camera
		{"far ", 	"%d",		"100"		},
		{"fov ", 	"%f",		"45"		},
		{"sensitivity ","%f",		"0.05"		},
	};

	unsigned int nelems = sizeof(paramArray)/sizeof(paramArray[0]);

	// create vectors
	for (int i = 0; i < nelems; i++)
		paramArray[i].target = vector_create(0, sizeof(void *));

	parse(fname, paramArray, nelems);

	Settings settings;

	for (int i = 0; i < nelems; i++){

		if (!strcmp(paramArray[i].name, "resolution ")){

			settings.width = *(int*)vector_get_element(paramArray[i].target, 0);
			settings.height = *(int*)vector_get_element(paramArray[i].target, 1);

		} else if (!strcmp(paramArray[i].name, "fullscreen ")){

			settings.fullscreen = *(int**)vector_get_element(paramArray[i].target, 0);

		} else if (!strcmp(paramArray[i].name, "msaa ")){

			settings.msaa = *(int*)vector_get_element(paramArray[i].target, 0);

		} else if (!strcmp(paramArray[i].name, "far ")){

			settings.far = *(int*)vector_get_element(paramArray[i].target, 0);

		} else if (!strcmp(paramArray[i].name, "fov ")){

			settings.fov = *(float*)vector_get_element(paramArray[i].target, 0);

		} else if (!strcmp(paramArray[i].name, "sensitivity ")){

			settings.sensitivity = *(float*)vector_get_element(paramArray[i].target, 0);

		}
	}

	// destroy vectors
	for (int i = 0; i < nelems; i++)
		vector_destroy(paramArray[i].target);

	return settings;
}

Mesh *parseMesh(const char *fname)
{
	Mesh *m = malloc(sizeof(Mesh));

	Vector *vver = NULL;
	Vector *vtex = NULL;
	Vector *vnorm = NULL;

	Parameter paramArray[] = {
		/* name 	fmt		def_val		delim */
		//Display
		{"v ", 		"%f %f %f",			NULL,		NULL},
		{"vt ", 	"%f %f",			NULL,		NULL},
		{"vn ", 	"%f %f %f",			NULL,		NULL},
		{"f ", 		"%d %d %d %d %d %d %d %d %d",	NULL,		"/"},
	};

	unsigned int nelems = sizeof(paramArray)/sizeof(paramArray[0]);

	// create vectors
	for (int i = 0; i < nelems; i++)
		paramArray[i].target = vector_create(0, sizeof(void *));

	parse(fname, paramArray, nelems);

	for (int i = 0; i < nelems; i++){

		if (!strcmp(paramArray[i].name, "v ")){

			vver = paramArray[i].target;
		} else if (!strcmp(paramArray[i].name, "vt ")){
			vtex = paramArray[i].target;
		} else if (!strcmp(paramArray[i].name, "vn ")){
			vnorm = paramArray[i].target;
		} else if (!strcmp(paramArray[i].name, "f ")){

			if (!(vver && vtex && vnorm)){
				LOGE("One of the vectors is NULL");
				//continue;
			}

			// 1 Vertex constains 3 elements
			m->vsize = vector_size(paramArray[i].target) / 3;
			m->isize = m->vsize;

			m->vertices = malloc(m->vsize * sizeof(Vertex));
			m->indices = malloc(m->isize * sizeof(*m->indices));

			for (int vertex = 0; vertex < m->vsize; vertex++){

				// position
				int posId = *(int*)vector_get_element(paramArray[i].target, vertex * 3);
				for (int offset = 0; offset < 3; offset++)
					m->vertices[vertex].position[offset] = *(float*)vector_get_element(vver, (posId - 1) * 3 + offset);

				// texels
				int texId = *(int*)vector_get_element(paramArray[i].target, (vertex * 3) + 1);
				for (int offset = 0; offset < 2; offset++)
					m->vertices[vertex].uv[offset] = *(float*)vector_get_element(vtex, (texId - 1) * 2 + offset);


				// normals

				// indices (I'm doing it wrong...)
				m->indices[vertex] = vertex;
			}
		}
	}

	// destroy vectors
	for (int i = 0; i < nelems; i++)
		vector_destroy(paramArray[i].target);

	return m;
}

unsigned int loadTexture(const char *fname)
{
	unsigned int ret;

	int width, height, nrChannels;
	unsigned char *data = stbi_load(fname, &width, &height, &nrChannels, 0);

	glGenTextures(1, &ret);
	glBindTexture(GL_TEXTURE_2D, ret);

	if (data) {

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	} else {
		LOGE("Failed to load texture \"%s\"", fname);

		//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1, 1, 0, GL_RGB, GL_UNSIGNED_BYTE, );
		//glGenerateMipmap(GL_TEXTURE_2D);
	}

	stbi_image_free(data);

	return ret;
}
