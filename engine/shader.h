#ifndef SHADER_H
#define SHADER_H

/*
shader utils
*/


unsigned int createShaderProgram(const char *vertexSourceFile, const char* fragmentSourceFile);

#endif
