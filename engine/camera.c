#include "camera.h"
#include <cglm/affine.h>
#include <cglm/cam.h>
#include <cglm/cglm.h>
#include <cglm/vec3.h>
#include <stdlib.h>
#include <string.h>

Camera *camera_create(float *position)
/* creates and initializes camera */
{
	Camera *c = calloc(1, sizeof(Camera));

	memcpy(c->front, (vec3){0, 0, -1}, sizeof(vec3));
	camera_translate(c, position);

	return c;
}

void camera_lookat(Camera *c, float *target)
/**/
{
	memcpy(c->target, target, sizeof(vec3));

	glm_vec3_sub(c->position, c->target, c->direction);
	glm_normalize(c->direction);

	glm_cross((vec3){0.0f, 1.0f, 0.0f}, c->direction, c->right);
	glm_normalize(c->right);

	glm_cross(c->direction, c->right, c->up);

	glm_lookat(c->position, c->target, c->up,  c->view);
}

void camera_translate(Camera *c, float *new_position)
/**/
{
	glm_vec3_add(c->position, new_position, c->position);
	glm_translate(c->view, c->position);
}
