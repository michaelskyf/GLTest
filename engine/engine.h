#ifndef ENGINE_H
#define ENGINE_H

#include "util.h"
#include "object.h"
#include "vector.h"
#include "camera.h"

typedef struct EngineVars {

	float deltaTime;

	Settings *settings;

	Camera **camera;
	GLFWwindow **window;
	Vector **shaderList;
	Vector **cameraList;

} EngineVars;

extern EngineVars engineVars;

void start();
void update();
#endif
