#ifndef OBJECT_H
#define OBJECT_H

#include <cglm/cglm.h>
#include <GLFW/glfw3.h>
#include "vector.h"

typedef struct Vertex {

	vec3 position;
	vec2 uv;
	vec3 normals;
} Vertex;

typedef struct Mesh{

	Vertex *vertices;
	unsigned int vsize;

	unsigned short *indices;
	unsigned int isize;
} Mesh;

typedef struct Object {

	Mesh *mesh;

	unsigned int vao, vbo, ebo;

	unsigned int texture;

	Vector *instanceList;
} Object;

typedef struct Shader{

	unsigned int ID;
	Vector *objectList;

}Shader;

typedef struct Instance{
	mat4 model;
}Instance;

void object_destroy(Object *o);
Object* object_create ();
void object_init (Object *o, Mesh *m);

unsigned int object_instance_create(Object *o);
void object_instance_destroy(Object *o, void *element);

void object_instance_scale(Instance *i, vec3 scalar);
void object_instance_move(Instance *i, vec3 vector);
void object_instance_move_make(Instance *i, vec3 vector);
#endif
