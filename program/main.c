#include "../engine/engine.h"
#include <cglm/affine.h>
#include <stdio.h>
#include <stdlib.h>

/* keybindings */

/* misc */
#define KEY_EXIT GLFW_KEY_Q

/* movement */
#define KEY_FORWARD GLFW_KEY_W
#define KEY_BACKWARD GLFW_KEY_S
#define KEY_LEFT GLFW_KEY_A
#define KEY_RIGHT GLFW_KEY_D


#define KEY_TEST1 GLFW_KEY_Z
#define KEY_TEST2 GLFW_KEY_X

/*
	This is the main file of the test program
	It has 2 functions:
	start() - gets called only one time after program start
	update() - gets called every frame
*/

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	switch(key){
		case KEY_EXIT:
			glfwSetWindowShouldClose(window, true);
			break;
		case KEY_TEST1:
			{
			       Shader *s = vector_get_element(*engineVars.shaderList, 0);
			       Object *o = vector_get_element(s->objectList, 0);

			       if (vector_size(o->instanceList) > 100){
			       for (int i = 0; i < 100; i++)
			       {
				object_instance_destroy(o, vector_end(o->instanceList));
			       }
			       }
			}
				break;
		case KEY_TEST2:
			{
			       Shader *s = vector_get_element(*engineVars.shaderList, 0);
			       Object *o = vector_get_element(s->objectList, 0);

			       for (int i = 0; i < 100; i++)
			       {
				unsigned int element = object_instance_create(o);

				float x = rand() % (100 - -100 + 1) -100;
				float y = rand() % (100 - -100 + 1) -100;
				float z = rand() % (100 - -100 + 1) -100;

				Instance *ins = vector_get_element(o->instanceList, element);
				object_instance_move_make(ins,  (vec3){x, y, z});
				float angle = rand() % 360;
				glm_rotate_x(ins->model, angle, ins->model);
				angle = rand() % 360;
				glm_rotate_y(ins->model, angle, ins->model);
				angle = rand() % 360;
				glm_rotate_z(ins->model, angle, ins->model);
			       }
			}
			break;
	}
}

void start()
{
	glfwSetKeyCallback(*engineVars.window, key_callback);

	Shader *s = vector_get_element(*engineVars.shaderList, 0);

	char *texFile = strcat_malloc(getCWD(), "textures/test.png");
	unsigned int texture = loadTexture(texFile);
	free(texFile);
	for (int i = 0; i < 1; i++) {
		Object *o = object_create();


		char *fname = strcat_malloc(getCWD(), "objects/cylinder.obj");
		Mesh *m = parseMesh(fname);
		free(fname);
		o->texture = texture;
		object_init(o, m);

		for(int ii = 0; ii < 16000; ii++)
		{
			object_instance_create(o);
		}
		vector_push(s->objectList, o);
		free(o);
	}

	for (int i = 0; i < vector_size(s->objectList); i++)
	{
		Object *o = vector_get_element(s->objectList, i);


		for(int ii = 0; ii < vector_size(o->instanceList); ii++)
		{
			float x = rand() % (100 - -100 + 1) -100;
			float y = rand() % (100 - -100 + 1) -100;
			float z = rand() % (100 - -100 + 1) -100;

			Instance *ins = vector_get_element(o->instanceList, ii);
			object_instance_move_make(ins,  (vec3){x, y, z});
			float angle = rand() % 360;
			glm_rotate_x(ins->model, angle, ins->model);
			angle = rand() % 360;
			glm_rotate_y(ins->model, angle, ins->model);
			angle = rand() % 360;
			glm_rotate_z(ins->model, angle, ins->model);
		}
	}
}

const float movementSpeed = 2.5f*5;
#define KEY(x) if (glfwGetKey(*engineVars.window, x) == GLFW_PRESS)
void update()
{
	/* movement */
	Camera *camera = *engineVars.camera;

	 KEY(KEY_FORWARD){
		vec3 v;
		glm_vec3_scale(camera->front, engineVars.deltaTime * movementSpeed, v);
		camera_translate(camera, v);
	 }
	KEY(KEY_BACKWARD){
		vec3 v;
		glm_vec3_scale(camera->front, -engineVars.deltaTime * movementSpeed, v);
		camera_translate(camera, v);
	}
	KEY(KEY_LEFT){
		vec3 v;
		glm_cross(camera->front, camera->up, v);
		glm_normalize(v);
		glm_vec3_scale(v, -engineVars.deltaTime * movementSpeed, v);
		camera_translate(camera, v);
	}
	KEY(KEY_RIGHT){
		vec3 v;
		glm_cross(camera->front, camera->up, v);
		glm_normalize(v);
		glm_vec3_scale(v, engineVars.deltaTime * movementSpeed, v);
		camera_translate(camera, v);
	}

	static float currentTime;
	static float lastTime;
	static float Time;
	static int frames;

	currentTime = glfwGetTime();

	float deltaTime = currentTime - lastTime;
	Time += deltaTime;

	if (Time >= 1) {
		LOG("FPS: %d/s", frames);
		frames = 0;
		Time = 0;
	}

	lastTime = currentTime;
	frames++;

}
