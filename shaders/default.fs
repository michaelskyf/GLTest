#version 330 core

in vec3 ocolor;
in vec2 TexCoord;

uniform sampler2D Texture;

out vec4 FragColor;

void main()
{
		FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f) * texture(Texture, TexCoord);
}
