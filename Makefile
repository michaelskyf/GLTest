TARGET = gltest

CC = cc
CFLAGS = -Wall -Werror -DSTBI_ONLY_PNG
CFLAGS += -O3 -g

# For Unix
LFLAGS = -lGL -lglfw -ldl -lm -lpthread
MKDIR = mkdir -p
RM = rm -rf
CP = cp -r

# For minGW
#CFLAGS += -I<include path>
#LFLAGS = -L<lib path> -static -lglfw3 -lopengl32 -mwindows
#RM = rm -r

export CC
export LD
export CFLAGS
export LFLAGS
export BUILDDIR = build


PROGRAM = $(BUILDDIR)/program.o
ENGINE = $(BUILDDIR)/engine.o

# Create build directory
BUILDDIRS += $(PROGRAM:%.o=%)
BUILDDIRS += $(ENGINE:%.o=%)

$(shell $(MKDIR) $(BUILDDIRS))

.PHONY: all clean release debug program engine

all: $(TARGET)

$(TARGET) : $(PROGRAM) $(ENGINE)
	$(CC) -o $@ $(PROGRAM) $(ENGINE) $(LFLAGS)

$(PROGRAM): FORCE
	$(MAKE) -C program

$(ENGINE): FORCE
	$(MAKE) -C engine

release: $(TARGET)
	$(MKDIR) Release
	$(CP) $(TARGET) LICENSE COPYING settings.conf objects textures shaders Release

clean:
	$(RM) Release Debug build $(TARGET)

FORCE: ;
